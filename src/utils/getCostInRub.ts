const getSign = (value: number) => (value > 0 ? '+' : '');

export const getCostInRub = (
  cost: number | null | undefined,
  defaultValue = '',
  withSign = false
): string => {
  // Хз стоит ли использовать полифилы
  const form = new Intl.NumberFormat('ru-Ru', {
    style: 'currency',
    currencyDisplay: 'symbol',
    useGrouping: true,
    currency: 'rub',
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
  });

  if (typeof cost !== 'number') {
    return defaultValue;
  }

  const textCost = form.format(cost);

  if (withSign) {
    return getSign(cost) + textCost;
  }

  return textCost;
};
