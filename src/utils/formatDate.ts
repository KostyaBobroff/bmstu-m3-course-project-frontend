// eslint-disable-next-line import/no-duplicates
import { format } from 'date-fns';
// eslint-disable-next-line import/no-duplicates
import { ru } from 'date-fns/locale';

export const formatDate = (date: Date, pattern: string): string => format(date, pattern, { locale: ru });
