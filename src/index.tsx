import 'regenerator-runtime/runtime';
import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'antd/dist/antd.css';
import App from './App';
import { observable } from 'mobx';

window.is502Error = observable.box(false);

ReactDOM.render(<App />, document.querySelector('#root'));
