import { IObservableValue } from 'mobx';

declare global {
  interface Window {
    IS_PRODUCTION: boolean;
    is502Error: IObservableValue<boolean>;
  }
}

export {};