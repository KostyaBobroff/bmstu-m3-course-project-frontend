const getApiUrl = () => process.env.API_URL || 'http://api.shuttleme.ru/' + '';

console.log('url', getApiUrl());
export default {
  history: `${getApiUrl()}history`, // история покупок
  tickets: `${getApiUrl()}prices_for_dates`, // поиск билетов
  profile: `${getApiUrl()}profile`, // флаги POST
  airports: `${getApiUrl()}airports`
};
