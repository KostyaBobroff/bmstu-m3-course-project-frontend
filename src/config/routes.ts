export enum InfoPageQueryParamEnum {
  user = 'user',
  tickets = 'tickets',
}

export const routes = {
  index: '/',
  info: {
    pattern: '/info',
    user: `/info?tab=${InfoPageQueryParamEnum.user}`,
    tickets: `/info?tab=${InfoPageQueryParamEnum.tickets}`,
  },
  auth: {
    login: '/login',
    registration: '/registration',
  },
};
