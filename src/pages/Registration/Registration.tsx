import { Button, Form, Input } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';
import { useHistory } from 'react-router-dom';

import AuthLayout from '../../components/AuthLayout';
import { routes } from '../../config/routes';
import rootStore from '../../store';

const Registration: React.FC = () => {
  const history = useHistory();
  const { userStore } = rootStore;

  const handleSubmit = React.useCallback(async (params: any) => {
    (await userStore.registration(params))
      ? history.replace(routes.index)
      : null;
  }, []);

  return (
    <AuthLayout title="Регистрация">
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        layout="horizontal"
        onFinish={handleSubmit}
      >
        <Form.Item
          label="Логин"
          name="username"
          rules={[{ required: true, message: 'Пожалуйста введите логин' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Пароль"
          name="password"
          rules={[{ required: true, message: 'Пожалуйста введите пароль!' }]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Имя"
          name="name"
          rules={[{ required: true, message: 'Пожалуйста введите имя' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Телефон"
          name="phone"
          rules={[{ required: true, message: 'Пожалуйста введите Телефон!' }]}
        >
          <Input placeholder="+7 999 99 99 99" type="tel" />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: 'Пожалуйста введите Email!' }]}
        >
          <Input placeholder="Ваш телефон" type="email" />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Button
            type="primary"
            htmlType="submit"
            loading={userStore.isLoading}
          >
            Зарегистрироваться
          </Button>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Button type="link" onClick={() => history.push(routes.auth.login)}>
            Если у вас есть аккаунт, то войдите через него
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
};

export default observer(Registration);
