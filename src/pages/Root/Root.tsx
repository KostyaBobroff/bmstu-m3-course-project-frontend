import { Layout, Spin } from 'antd';
import { toJS } from 'mobx';
import { observer } from 'mobx-react';
import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router';

import { routes } from 'config/routes';
import FindTickets from 'pages/FindTickets';
import Info from 'pages/Info';
import Login from 'pages/Login';
import Registration from 'pages/Registration';

import BadGatewayResult from '../../components/BadGatewayResult';
import PrivateRoute from '../../components/PrivateRoute';
import rootStore from '../../store';

const Root: React.FC = () => {
  const { userStore } = rootStore;

  React.useEffect(() => {
    userStore.loadProfile();
  }, []);

  if (userStore.isAuthorizing) {
    return (
      <Spin
        size="large"
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      />
    );
  }



  if (window.is502Error.get()) {
    return <BadGatewayResult />;
  }

  return (
    <Switch>
      <Route path={routes.index} exact component={FindTickets} />
      {/*<Route path={routes.info.pattern} exact component={Info} />*/}

      <PrivateRoute path={routes.info.pattern}>
        <Info />
      </PrivateRoute>
      <Route path={routes.auth.login}>
        <Login />
      </Route>
      <Route path={routes.auth.registration}>
        <Registration />
      </Route>
      <Redirect to={routes.index} />
    </Switch>
  );
};

export default observer(Root);
