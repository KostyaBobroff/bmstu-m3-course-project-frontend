import { Space, Tag, Typography } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';

import TicketCard from 'components/TicketCard';
import { HistoryTicketModel } from 'store/HistoryTicketsStore';

type Props = {
  model: HistoryTicketModel;
};

const HistoryTicketCard: React.FC<Props> = ({ model }) => {
  return (
    <TicketCard
      model={model}
      full
      withoutIndent
      action={
        <Typography.Text>
          <Space direction="vertical">
            <b>{model.fullName}</b>
            {/*<br />*/}
            {!model.isPaid ? (
              <Tag color="red">Не оплачено</Tag>
            ) : (
              <Tag color="green">Оплачено</Tag>
            )}
          </Space>
        </Typography.Text>
      }
    />
  );
};

export default observer(HistoryTicketCard);
