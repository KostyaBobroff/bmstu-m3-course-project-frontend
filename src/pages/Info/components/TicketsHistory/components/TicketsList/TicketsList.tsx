import { Space, Spin } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';

import { HistoryTicketsStore } from 'store/HistoryTicketsStore';

import styles from './TicketsList.modules.scss';
import HistoryTicketCard from './components/HistoryTicketCard';

const TicketsList: React.FC = () => {
  const [historyTicketsStore] = React.useState(() => new HistoryTicketsStore());

  React.useEffect(() => {
    historyTicketsStore.loadHistory();
  }, [historyTicketsStore]);

  if (historyTicketsStore.meta.isLoading) {
    return <Spin size="large" />;
  }

  return (
    <Space direction="vertical" className={styles.space} size="large">
      {historyTicketsStore.tickets.map((elem) => (
        <HistoryTicketCard model={elem} key={elem.id} />
      ))}
    </Space>
  );
};

export default observer(TicketsList);
