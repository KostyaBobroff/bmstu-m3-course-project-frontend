import { Typography } from 'antd';
import * as React from 'react';

import styles from './TicketsHistory.modules.scss';
import TicketsList from './components/TicketsList';

const { Title } = Typography;

const TicketsHistory: React.FC = () => {
  return (
    <div className={styles.wrapper}>
      <Title level={2}>Купленные билеты</Title>
      <TicketsList />
    </div>
  );
};

export default TicketsHistory;
