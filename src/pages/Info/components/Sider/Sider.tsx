import { Menu, Layout } from 'antd';
import * as React from 'react';
import { useHistory } from 'react-router-dom';

import { InfoPageQueryParamEnum, routes } from 'config/routes';
import { useQuery } from 'hooks/useQuery';

const { Sider: AntdSider } = Layout;

const Sider: React.FC = () => {
  const history = useHistory();
  const queryParamas = useQuery();
  const activeKey = queryParamas.get('tab') as InfoPageQueryParamEnum;

  return (
    <AntdSider width={350}>
      <Menu
        mode="inline"
        theme="dark"
        defaultSelectedKeys={[activeKey]}
        style={{ height: '100%' }}
      >
        <Menu.Item
          key={InfoPageQueryParamEnum.user}
          onClick={() => history.replace(routes.info.user)}
        >
          Пользователь
        </Menu.Item>
        <Menu.Item
          key={InfoPageQueryParamEnum.tickets}
          onClick={() => history.replace(routes.info.tickets)}
        >
          История билетов
        </Menu.Item>
      </Menu>
    </AntdSider>
  );
};

export default Sider;
