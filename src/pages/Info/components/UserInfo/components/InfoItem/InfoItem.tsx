import { Typography } from 'antd';
import * as React from 'react';

import styles from './InfoItem.modules.scss';

type Props = {
  label: string;
  content: string;
};

const InfoItem: React.FC<Props> = ({ label, content }) => {
  return (
    <div>
      <Typography.Title level={4} className={styles['info-item__header']}>
        {label}:{' '}
      </Typography.Title>
      <Typography className={styles['info-item__value']}>
        {content}
      </Typography>
    </div>
  );
};

export default React.memo(InfoItem);
