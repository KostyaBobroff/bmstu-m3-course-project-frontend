import { Button, Form, Input, Space, Spin } from 'antd';
import Title from 'antd/es/typography/Title';
import { observer } from 'mobx-react';
import * as React from 'react';

import rootStore from '../../../../store';
import { UserProfileType } from '../../../../store/UserStore/types';

const UserInfo: React.FC = () => {
  const { userStore } = rootStore;

  const [edit, setEdit] = React.useState(false);
  const [form] = Form.useForm<UserProfileType>();

  const onToggleEdit = React.useCallback(() => {
    setEdit((e) => !e);
  }, []);
  const profile = userStore.profile;

  React.useEffect(() => {
    if (!profile) {
      return;
    }

    form.setFieldsValue(profile);
  }, [profile, form]);

  const onFinish = React.useCallback(
    (values: UserProfileType) => {
      userStore.editProfile(values);
      onToggleEdit();
    },
    [userStore, onToggleEdit]
  );

  if (userStore.isLoading) {
    return <Spin size="large" />;
  }

  if (!profile) {
    return null;
  }

  return (
    <div>
      <Title level={2}>Профиль пользователя</Title>
      <Space size="middle" direction="vertical">
        <Button
          type="primary"
          danger={edit}
          onClick={onToggleEdit}
          loading={userStore.isLoading}
        >
          {!edit ? 'Редактировать' : 'Отмена'}
        </Button>
        <Form<UserProfileType>
          form={form}
          onFinish={onFinish}
          layout="horizontal"
        >
          <Form.Item
            label="Логин"
            name="username"
            rules={[{ required: true, message: 'Пожалуйста введите логин' }]}
          >
            <Input disabled={!edit} />
          </Form.Item>
          <Form.Item
            label="Имя"
            name="name"
            rules={[{ required: true, message: 'Пожалуйста введите имя' }]}
          >
            <Input disabled={!edit} />
          </Form.Item>
          <Form.Item
            label="Телефон"
            name="phone"
            rules={[{ required: true, message: 'Пожалуйста введите Телефон!' }]}
          >
            <Input placeholder="+7 999 99 99 99" type="tel" disabled={!edit} />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Пожалуйста введите Email!' }]}
          >
            <Input placeholder="Ваш телефон" type="email" disabled={!edit} />
          </Form.Item>
          {edit && (
            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
              <Button
                type="primary"
                htmlType="submit"
                loading={userStore.isLoading}
              >
                Сохранить изменения
              </Button>
            </Form.Item>
          )}
        </Form>
      </Space>
    </div>
  );
};

export default observer(UserInfo);
