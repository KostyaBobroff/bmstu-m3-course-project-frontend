import * as React from 'react';

import Layout from 'components/Layout';

import { InfoPageQueryParamEnum } from '../../config/routes';
import { useQuery } from '../../hooks/useQuery';

import Sider from './components/Sider';
import TicketsHistory from './components/TicketsHistory';
import UserInfo from './components/UserInfo';

const ComponentsMap: Record<InfoPageQueryParamEnum, React.ComponentType> = {
  [InfoPageQueryParamEnum.tickets]: TicketsHistory,
  [InfoPageQueryParamEnum.user]: UserInfo,
};

const Info: React.FC = () => {
  const searchParams = useQuery();
  const param: InfoPageQueryParamEnum = searchParams.get(
    'tab'
  ) as InfoPageQueryParamEnum;

  const Component: React.ComponentType | undefined = ComponentsMap[param];

  const sider = React.useMemo(() => <Sider />, []);

  if (!Component) {
    return null;
  }

  return (
    <Layout leftSider={sider} contentScroll>
      <Component />
    </Layout>
  );
};

export default Info;
