import { Spin } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';

import Layout from 'components/Layout';
import { FindTicketsPageStoreProvider } from 'store/FindTicketsPageStore';

import rootStore from '../../store';

import FindTicketsForm from './component/FindTicketsForm';
import TicketsList from './component/TicketsList';

const FindTickets: React.FC = () => {
  const {airportsStore} = rootStore;

  React.useEffect(() => {
    airportsStore.loadAirports();
  }, []);

  if (airportsStore.meta.isLoading) {
    return (
      <Spin
        size="large"
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
      />
    );
  }

  return (
    <FindTicketsPageStoreProvider>
      <Layout fixedHeader>
        <div>
          <FindTicketsForm />
          <TicketsList />
        </div>
      </Layout>
    </FindTicketsPageStoreProvider>
  );
};

export default observer(FindTickets);
