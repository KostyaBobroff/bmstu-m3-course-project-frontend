import { DatePicker } from 'antd';
import { PickerProps } from 'antd/lib/date-picker/generatePicker';
import { Moment } from 'moment';
import * as React from 'react';

const DepartureDatePicker: React.FC<PickerProps<Moment>> = (props) => {
  return (
    <DatePicker
      {...props}
      placeholder="Выберете дату"
      disabledDate={(date: Moment) => {
        return date.isBefore(new Date(), 'day');
      }}
    />
  );
};

export default DepartureDatePicker;
