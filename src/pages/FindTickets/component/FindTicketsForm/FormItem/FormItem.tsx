import { Form } from 'antd';
import { FormItemProps } from 'antd/lib/form/FormItem';
import * as React from 'react';

import styles from './FormItem.modules.scss';

function FormItem<Values = any>(
  props: FormItemProps<Values>
): React.ReactElement {
  return (
    <Form.Item<Values>
      {...props}
      className={styles.formItem}
      rules={[{ required: true }]}
    >
      {props.children}
    </Form.Item>
  );
}

export default FormItem;
