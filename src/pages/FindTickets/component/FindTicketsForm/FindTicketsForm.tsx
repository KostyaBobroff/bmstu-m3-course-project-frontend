import { Button, Form } from 'antd';
import * as React from 'react';

import { useFindTicketsPageStore } from 'store/FindTicketsPageStore';

import DepartureDatePicker from './DepartureDatePicker';
import styles from './FindTicketsForm.modules.scss';
import FormItem from './FormItem';
import SelectAirports from './SelectAirports';

const FindTicketsForm: React.FC = () => {
  const validateMessages = {
    // eslint-disable-next-line no-template-curly-in-string
    required: 'Поле "${label}" пустое',
  };

  const store = useFindTicketsPageStore();

  const onSubmit = React.useCallback((values: any) => {
    store.loadTickets({
      currency: 'rub',
      departureAt: values.departureTime.toDate(),
    });
  }, []);

  return (
    <div className={styles.formWrapper}>
      <Form
        layout="inline"
        className={styles.form}
        colon={false}
        onFinish={onSubmit}
        validateMessages={validateMessages}
      >
        <FormItem label="Откуда" name="from">
          <SelectAirports placeholder="Откуда" />
        </FormItem>
        <FormItem label="Куда" name="to">
          <SelectAirports placeholder="Куда" />
        </FormItem>
        <FormItem label="Когда" name="departureTime">
          <DepartureDatePicker />
        </FormItem>
        <FormItem>
          <Button type="primary" className={styles.button} htmlType="submit">
            Найти билеты
          </Button>
        </FormItem>
      </Form>
    </div>
  );
};

export default FindTicketsForm;
