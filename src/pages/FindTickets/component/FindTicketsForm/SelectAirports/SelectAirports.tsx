import { Select, Typography } from 'antd';
import { RefSelectProps, SelectProps, SelectValue } from 'antd/lib/select';
import { observer } from 'mobx-react';
import React from 'react';

import rootStore from '../../../../../store';

const { Option } = Select;

function SelectAirports<VT extends SelectValue = SelectValue>(
  props: SelectProps<VT> & {
    ref?: React.Ref<RefSelectProps> | undefined;
  }
): React.ReactElement {
  const { airportsStore } = rootStore;

  return (
    <Select
      {...props}
      showSearch
      optionFilterProp="children"
      style={{ width: 175 }}
      filterOption={(input, option: any) =>
        option?.airportName.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
        option?.cityName.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
      filterSort={(optionA, optionB) => {
        return (
          optionA.airportName
            .toLowerCase()
            .localeCompare(optionB.airportName.toLowerCase()) ||
          optionA.cityName
            .toLowerCase()
            .localeCompare(optionB.cityName.toLowerCase())
        );
      }}
    >
      {airportsStore.airports.map(({ iata, cityName, airportName }) => (
        <Option
          value={iata}
          key={iata}
          airportName={airportName}
          cityName={cityName}
        >
          <Typography.Text>{airportName}</Typography.Text>
          <br />
          <Typography.Text type="secondary">{cityName}</Typography.Text>
        </Option>
      ))}
    </Select>
  );
}

export default observer(SelectAirports);
