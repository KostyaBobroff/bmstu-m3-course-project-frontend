import { Empty, Space, Spin, Typography } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';

import { useFindTicketsPageStore } from 'store/FindTicketsPageStore';

import styles from './TicketList.modules.scss';
import BuyTicketCard from './components/BuyTicketCard';

const TicketsList: React.FC = () => {
  const store = useFindTicketsPageStore();

  console.log(store);
  return (
    <div className={styles.ticketListWrapper}>
      <div className={styles.ticketList}>
        <Space direction="vertical" size="large" className={styles.space}>
          {store.isLoading ? (
            <div className={styles.spinnerWrapper}>
              <Spin size="large" className={styles.spinner} />
            </div>
          ) : store.tickets.length > 0 ? (
            store.tickets.map((elem) => (
              <BuyTicketCard key={elem.id} model={elem} />
            ))
          ) : (
            <Empty
              description={
                <Typography>
                  Нету билетов, которые удовлетворяют фильтрам
                </Typography>
              }
            />
          )}
        </Space>
      </div>
    </div>
  );
};

export default observer(TicketsList);
