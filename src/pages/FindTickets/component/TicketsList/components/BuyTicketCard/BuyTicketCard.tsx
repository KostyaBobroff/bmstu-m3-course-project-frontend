import { Button } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';
import { useHistory } from 'react-router-dom';

import TicketCard from 'components/TicketCard';
import { TicketModel } from 'store/models/TicketModel';

import { routes } from '../../../../../../config/routes';
import rootStore from '../../../../../../store';

import BuyModal from './BuyModal';

type Props = {
  model: TicketModel;
};

const BuyTicketCard: React.FC<Props> = ({ model }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const { userStore } = rootStore;
  const history = useHistory();

  return (
    <>
      <BuyModal
        model={model}
        isOpen={isOpen}
        onCancel={() => setIsOpen(false)}
      />
      <TicketCard
        model={model}
        action={
          <Button
            shape="round"
            size="large"
            block
            type="primary"
            onClick={() =>
              userStore.isAuthorized
                ? setIsOpen(true)
                : history.push(routes.auth.login)
            }
          >
            Купить
          </Button>
        }
      />
    </>
  );
};

export default observer(BuyTicketCard);
