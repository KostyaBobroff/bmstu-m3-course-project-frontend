import { PlusOutlined } from '@ant-design/icons';
import {
  Button,
  Divider,
  Form,
  Input,
  Modal,
  notification,
  Space,
  Typography,
} from 'antd';
import { isBefore } from 'date-fns';
import { observer } from 'mobx-react';
import * as React from 'react';

import DateFnsDatePicker from 'components/DateFnsDatePicker';
import { BuyFormParams, TicketModel } from 'store/models/TicketModel';

import { formatDate } from '../../../../../../utils/formatDate';
import { getCostInRub } from '../../../../../../utils/getCostInRub';

import styles from './BuyModal.modules.scss';

type FormParamsType = { users: BuyFormParams[] };
type Props = {
  model: TicketModel;
  onCancel: VoidFunction;
  isOpen: boolean;
};

const BuyModal: React.FC<Props> = ({ model, isOpen, onCancel }) => {
  const [form] = Form.useForm();

  const [passengersCount, setPassengersCount] = React.useState<number>(0);

  const increasePassengersCount = React.useCallback(() => {
    setPassengersCount((el) => ++el);
  }, [setPassengersCount]);

  const decreasePassengersCount = React.useCallback(() => {
    setPassengersCount((el) => --el);
  }, [setPassengersCount]);

  const onSubmit = React.useCallback(
    async (values: FormParamsType) => {
      const result = await model.buyTicket(values.users);
      if (result) {
        notification.success({
          message: 'Билет куплен',
          description:
            'Билет был куплен успешно, он отобразится в разделе "Купленные билеты".',
        });
      } else {
        notification.error({
          message: 'Ошибка',
          description:
            'Что-то пошло не так, не удалось купить билет. Пожалуйста попробуйте позже или обратитесь в службу поддержки.',
        });
      }
      onCancel();
    },
    [model.buyTicket]
  );

  React.useEffect(() => {
    !isOpen && setPassengersCount(0);
  }, [isOpen]);

  return (
    <Modal
      title={`Покупка билетов ${model.flightNumber} на ${
        model.departureInfo.date &&
        formatDate(model.departureInfo.date, 'dd.LL.y')
      }`}
      onCancel={onCancel}
      afterClose={Modal.destroyAll}
      onOk={form.submit}
      okButtonProps={{
        disabled: !passengersCount,
        loading: model.meta.isLoading,
      }}
      visible={isOpen}
      confirmLoading={model.meta.isLoading}
      okText="Купить"
      cancelText="Отмена"
      destroyOnClose
    >
      <Form<FormParamsType>
        form={form}
        layout="horizontal"
        onFinish={onSubmit}
        preserve={false}
      >
        <Form.List name="users">
          {(fields, { add, remove }) => (
            <>
              {fields.map(({ key, name, ...restField }) => (
                <React.Fragment key={key}>
                  <Form.Item
                    {...restField}
                    name={[name, 'name']}
                    label="Имя"
                    rules={[{ required: true, message: 'Введите имя' }]}
                  >
                    <Input placeholder="Имя" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, 'sirname']}
                    label="Фамилия"
                    rules={[{ required: true, message: 'Введите фамилию' }]}
                  >
                    <Input placeholder="Фамилия" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, 'patronymic']}
                    label="Отчество"
                    rules={[{ required: true, message: 'Введите отчество' }]}
                  >
                    <Input placeholder="Отчество" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, 'passwordSeries']}
                    label="Серия паспорта"
                    rules={[
                      { required: true, message: 'Введите серию паспорта' },
                    ]}
                  >
                    <Input placeholder="Серия паспорта" type="number" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, 'passwordNumbers']}
                    label="Номер паспорта"
                    rules={[
                      { required: true, message: 'Введите серию паспорта' },
                    ]}
                  >
                    <Input placeholder="Номер паспорта" type="number" />
                  </Form.Item>
                  <Form.Item
                    {...restField}
                    name={[name, 'birthDate']}
                    label="Дата рождения"
                    rules={[
                      { required: true, message: 'Введите Дату рождения' },
                    ]}
                  >
                    <DateFnsDatePicker
                      placeholder="Выберете дату"
                      disabledDate={(date: Date) => {
                        return isBefore(new Date(), date);
                      }}
                    />
                  </Form.Item>
                  <Button
                    danger
                    type="primary"
                    onClick={() => {
                      remove(name);
                      decreasePassengersCount();
                    }}
                  >
                    Удалить
                  </Button>
                  <Divider />
                </React.Fragment>
              ))}
              <Form.Item>
                <Button
                  type="dashed"
                  onClick={() => {
                    add();
                    increasePassengersCount();
                  }}
                  block
                  icon={<PlusOutlined />}
                >
                  Добавить пассажира
                </Button>
              </Form.Item>
            </>
          )}
        </Form.List>
      </Form>

      {passengersCount > 0 && (
        <Space direction="vertical">
          <Typography.Text>Сумма:</Typography.Text>
          <Typography.Title level={4}>{`${getCostInRub(
            model.cost
          )} x ${passengersCount} = ${getCostInRub(
            model.cost * passengersCount
          )}`}</Typography.Title>
        </Space>
      )}
    </Modal>
  );
};

export default observer(BuyModal);
