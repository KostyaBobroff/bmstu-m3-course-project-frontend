import { Button, Form, Input } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';
import { useHistory } from 'react-router-dom';

import AuthLayout from 'components/AuthLayout';
import { routes } from 'config/routes';

import rootStore from '../../store';

const Login: React.FC = () => {
  const history = useHistory();
  const { userStore } = rootStore;

  const handleSubmit = React.useCallback(async (params: any) => {
    (await userStore.login(params)) ? history.replace(routes.index) : null;
  }, []);

  return (
    <AuthLayout title="Вход">
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        layout="horizontal"
        onFinish={handleSubmit}
      >
        <Form.Item
          label="Логин"
          name="username"
          rules={[{ required: true, message: 'Пожалуйста введите логин' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Пароль"
          name="password"
          rules={[{ required: true, message: 'Пожалуйста введите пароль!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
          <Button
            type="primary"
            htmlType="submit"
            loading={userStore.isLoading}
          >
            Войти
          </Button>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 10, span: 14 }}>
          <Button
            type="link"
            onClick={() => history.push(routes.auth.registration)}
          >
            Регистрация
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
};

export default observer(Login);
