import { observer } from 'mobx-react';
import React from 'react';
import { Route, RouteProps, Redirect } from 'react-router';
import rootStore from '../../store';


const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const isAuthorized = rootStore.userStore.isAuthorized;

  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthorized ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default observer(PrivateRoute);
