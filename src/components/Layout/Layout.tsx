import { Button, Layout as AntdLayout, Space, Typography } from 'antd';
import cn from 'classnames';
import { observer } from 'mobx-react';
import * as React from 'react';
import { Link, useHistory } from 'react-router-dom';

import { routes } from '../../config/routes';
import rootStore from '../../store';

import styles from './Layout.modules.scss';

interface Props {
  fixedHeader?: boolean;
  leftSider?: React.ReactNode;
  contentScroll?: boolean;
}

const Layout: React.FC<Props> = ({
  children,
  fixedHeader = false,
  leftSider,
  contentScroll = false,
}) => {
  const history = useHistory();
  const { userStore } = rootStore;

  return (
    <AntdLayout className={styles['layout']}>
      <AntdLayout.Header
        className={cn(
          styles['layout__header'],
          fixedHeader && styles['layout__header_fixed']
        )}
      >
        <Link to={routes.index} className={styles['layout__logo']}>
          <Typography.Title
            title="3"
            className={styles['layout__logo-content']}
          >
            Flying Sales
          </Typography.Title>
        </Link>
        <Space>
          <Button
            ghost
            onClick={() =>
              history.push(
                userStore.isAuthorized ? routes.info.user : routes.auth.login
              )
            }
          >
            {userStore.isAuthorized ? 'Профиль' : 'Войти'}
          </Button>
          {userStore.isAuthorized && (
            <Button ghost type="primary" danger onClick={userStore.logout}>
              Выйти
            </Button>
          )}
        </Space>
      </AntdLayout.Header>
      <AntdLayout>
        {leftSider}
        <AntdLayout.Content
          className={cn(
            styles['layout__content'],
            contentScroll && styles['layout__content_scroll']
          )}
        >
          {children}
        </AntdLayout.Content>
      </AntdLayout>
    </AntdLayout>
  );
};

export default observer(Layout);
