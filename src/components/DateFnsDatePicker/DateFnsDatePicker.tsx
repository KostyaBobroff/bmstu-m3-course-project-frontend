import generatePicker from 'antd/es/date-picker/generatePicker';
import dateFnsGenerateConfig from 'rc-picker/lib/generate/dateFns';

const DateFnsDatePicker = generatePicker<Date>(dateFnsGenerateConfig);

export default DateFnsDatePicker;