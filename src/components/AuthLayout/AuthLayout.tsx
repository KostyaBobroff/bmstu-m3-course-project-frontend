import { Card, Layout } from 'antd';
import React from 'react';

import styles from './AuthLayout.modules.scss';

type Props = {
  children: React.ReactNode;
  title: string;
};

const AuthLayout: React.FC<Props> = ({ children, title }) => {
  return (
    <Layout className={styles['auth-layout']}>
      <Card title={title} bordered className={styles['auth-layout__card']}>
        {children}
      </Card>
    </Layout>
  );
};

export default AuthLayout;
