import { Typography, Space } from 'antd';
import cn from 'classnames';
import { observer } from 'mobx-react';
import * as React from 'react';

import { TicketModel } from 'store/models/TicketModel';

import { getCostInRub } from '../../utils/getCostInRub';

import styles from './TicketCard.modules.scss';
import TicketInfo from './components/TicketInfo';

const { Title } = Typography;

type Props = {
  className?: string;
  action?: React.ReactNode;
  full?: boolean;
  model: TicketModel;
  withoutIndent?: boolean;
};

const TicketCard: React.FC<Props> = ({
  className,
  model,
  action,
  full = false,
  withoutIndent = false,
}) => {
  return (
    <div
      className={cn(
        styles.ticketCard,
        full && styles.leftWrapper_full,
        className
      )}
    >
      <div className={styles.leftWrapper}>
        <Title level={3}>{getCostInRub(model.cost)}</Title>
        {!withoutIndent && <div className={styles.indent} />}
        {action}
      </div>
      <div className={styles.airInfoWrapper}>
        <Space direction="vertical" size="large" className={styles.space}>
          <Title level={4}>{model.flyingCompanyName}</Title>
          <TicketInfo model={model} />
        </Space>
      </div>
    </div>
  );
};

export default observer(TicketCard);
