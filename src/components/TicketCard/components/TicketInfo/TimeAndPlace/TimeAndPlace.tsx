import { Typography } from 'antd';
import { format } from 'date-fns';
import * as React from 'react';

import { TicketInfo } from '../../../../../store/models/TicketModel';
import { formatDate } from '../../../../../utils/formatDate';

import styles from './TimeAndPlace.modules.scss';

const { Title, Text } = Typography;

type Props = {
  className?: string;
  info: TicketInfo;
};
const TimeAndPlace: React.FC<Props> = ({ className, info }) => {
  return (
    <div className={className}>
      <Title level={5}>{info.time && formatDate(info.time, 'H:mm')}</Title>
      <Text type="secondary" className={styles.text}>
        {info.city}
      </Text>
      <Text type="secondary" className={styles.text}>
        {info.date && formatDate(info.date, 'd MMM y, EEEEEE')}
      </Text>
    </div>
  );
};

export default TimeAndPlace;
