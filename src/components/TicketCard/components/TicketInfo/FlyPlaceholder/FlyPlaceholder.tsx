import { Typography } from 'antd';
import cn from 'classnames';

import * as React from 'react';

import { TicketModel } from 'store/models/TicketModel';

import styles from './FlyPlaceholder.modules.scss';

const { Text } = Typography;

type Props = {
  className?: string;
  model: TicketModel;
};
const FlyPlaceholder: React.FC<Props> = ({ className, model }) => {
  return (
    <div className={cn(styles.flyPlaceholder, className)}>
      <Text type="secondary" className={styles.text}>
        {/*В пути: {format(model.timeInFlying, 'Hч mм')}*/}В пути:{' '}
        {model.timeInFlying}
      </Text>
      <Text strong className={styles.text}>
        {model.departureAirportCode} {'->'} {model.arrivalAirportCode}
      </Text>
    </div>
  );
};

export default FlyPlaceholder;
