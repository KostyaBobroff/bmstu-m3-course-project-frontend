import * as React from 'react';

import { TicketModel } from 'store/models/TicketModel';

import FlyPlaceholder from './FlyPlaceholder';
import styles from './TicketInfo.modules.scss';
import TimeAndPlace from './TimeAndPlace';

type Props = {
  model: TicketModel;
};

const TicketInfo: React.FC<Props> = ({ model }) => {
  return (
    <div className={styles.ticketInfo}>
      <TimeAndPlace info={model.departureInfo} />
      <FlyPlaceholder className={styles.placeholder} model={model} />
      <TimeAndPlace info={model.arrivalInfo} />
    </div>
  );
};

export default TicketInfo;
