import { Button, Result } from 'antd';
import { observer } from 'mobx-react';
import * as React from 'react';
import { useHistory } from 'react-router-dom';

import { routes } from '../../config/routes';

const BadGatewayResult = () => {
  const history = useHistory();

  const onBack = React.useCallback(() => {
    window.is502Error.set(false);
    history.replace(routes.index);
  }, [history]);

  return (
    <Result
      status="500"
      title="502"
      subTitle="Упс, что-то пошло не так, попробуйте позже"
      extra={
        <Button type="primary" onClick={onBack}>
          Вернуться на страницу поиска билетов
        </Button>
      }
    />
  );
};

export default observer(BadGatewayResult);
