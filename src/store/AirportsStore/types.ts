export type AirportResponseType = {
  city_code: string;
  iata: string;
  name: string;
  name_translations: {
    ru: string;
  };
};

export type AirportType = {
  cityCode: string;
  iata: string;
  airportName: string;
  cityName: string;
};
