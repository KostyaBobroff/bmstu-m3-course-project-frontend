import { action, computed, makeObservable, observable } from 'mobx';

import urls from '../../config/urls';
import api from '../../utils/api';
import { MetaStore } from '../common/MetaStore';
import { ApiResponse } from '../types';

import { normalizeAirportResponse } from './normalizers';
import { AirportResponseType, AirportType } from './types';

type PrivateFields = '_airports' | '_setAirports';

export class AirportsStore {
  readonly meta = new MetaStore();

  private _airports: AirportType[] = [];

  constructor() {
    makeObservable<AirportsStore, PrivateFields>(this, {
      _airports: observable.ref,

      airports: computed,

      loadAirports: action.bound,
      _setAirports: action.bound,
    });
  }

  get airports(): AirportType[] {
    return this._airports;
  }

  private _setAirports(newAirports: AirportType[]): void {
    this._airports = newAirports;
  }

  async loadAirports(): Promise<void> {
    this.meta.setLoadedStartMeta();
    const { response }: ApiResponse<AirportResponseType[]> = await api(
      urls.airports,
      'GET'
    );

    if (response) {
      this._setAirports(normalizeAirportResponse(response));
    }
    this.meta.setLoadedSuccessMeta();
  }
}
