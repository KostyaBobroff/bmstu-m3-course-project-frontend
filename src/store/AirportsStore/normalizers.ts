import { AirportResponseType, AirportType } from './types';

export const normalizeAirportResponse = (
  data: AirportResponseType[]
): AirportType[] => {
  return data.map((elem) => ({
    cityCode: elem.city_code,
    airportName: elem.name,
    iata: elem.iata,
    cityName: elem.name_translations.ru,
  }));
};
