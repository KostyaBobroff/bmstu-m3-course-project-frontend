import * as React from 'react';

import { FindTicketsPageStore } from './FindTicketsStore';

const FindTicketsPageStoreContext =
  React.createContext<FindTicketsPageStore | null>(null);

export const FindTicketsPageStoreProvider: React.FC = ({ children }) => {
  const store = new FindTicketsPageStore();
  return (
    <FindTicketsPageStoreContext.Provider value={store}>
      {children}
    </FindTicketsPageStoreContext.Provider>
  );
};

export const useFindTicketsPageStore = (): FindTicketsPageStore => {
  const ctx = React.useContext<FindTicketsPageStore | null>(
    FindTicketsPageStoreContext
  );

  if (!ctx) {
    throw new Error('doesn t exist FindTicketsPageStore');
  }

  return ctx;
};
