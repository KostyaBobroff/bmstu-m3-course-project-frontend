import { action, computed, makeObservable, observable } from 'mobx';

import urls from '../../config/urls';
import api from '../../utils/api';
import { formatDate } from '../../utils/formatDate';
import { MetaStore } from '../common/MetaStore';
import { TicketModel, TicketParams } from '../models/TicketModel';
import { ApiResponse } from '../types';

import { FindTicketsParams } from './types';

type PrivateFields = '_tickets' | '_setTickets';

export class FindTicketsPageStore {
  private _tickets: TicketModel[] = [];
  readonly meta = new MetaStore();

  constructor() {
    makeObservable<FindTicketsPageStore, PrivateFields>(this, {
      _tickets: observable.ref,

      tickets: computed,
      isLoading: computed,

      loadTickets: action.bound,
      _setTickets: action,
    });
  }

  get tickets(): TicketModel[] {
    return this._tickets;
  }

  get isLoading(): boolean {
    return this.meta.isLoading;
  }

  private _setTickets(newTickets: TicketModel[]): void {
    this._tickets = newTickets;
  }

  async loadTickets({
    currency = 'rub',
    departureAt,
  }: FindTicketsParams): Promise<void> {
    this.meta.setLoadedStartMeta();
    try {
      const { response }: ApiResponse<TicketParams[]> = await api(
        urls.tickets,
        'GET',
        {
          currency,
          departure_at: formatDate(departureAt, 'y-M-d'),
        }
      );

      if (response) {
        console.log(response)
        this._setTickets(response.map((elem) => new TicketModel(elem)));
      }
    } catch (e) {
      console.log(e);
    }
    this.meta.setLoadedSuccessMeta();
  }
}
