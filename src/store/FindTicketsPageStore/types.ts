export type FindTicketsParams = {
  currency: string;
  departureAt: Date;
}

export type FindTicketsRequest = {
  currency: string;
  departure_at: string;
}