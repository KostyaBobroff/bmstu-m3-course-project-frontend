import { action, computed, makeObservable, observable } from 'mobx';

type PrivateFields = '_isLoading' | '_isError' | '_isLoaded';

/**
 * Mobx класс для флажков isLoading и isError, Возможно потом будет расширяться
 **/
export class MetaStore {
  private _isLoading: boolean;
  private _isError: boolean;
  private _isLoaded: boolean;

  constructor() {
    this._isError = false;
    this._isLoading = false;
    makeObservable<MetaStore, PrivateFields>(this, {
      _isError: observable,
      _isLoading: observable,
      _isLoaded: observable,

      isLoading: computed,
      isError: computed,
      isLoaded: computed,
      isInitialMetaState: computed,

      setLoadedStartMeta: action,
      setLoadedSuccessMeta: action,
      setLoadedErrorMeta: action,
    });
  }

  get isLoading(): boolean {
    return this._isLoading;
  }

  get isError(): boolean {
    return this._isError;
  }

  get isInitialMetaState(): boolean {
    return !this.isLoading && !this.isLoaded && !this.isError;
  }

  get isLoaded(): boolean {
    return this._isLoaded;
  }

  setLoadedStartMeta(): void {
    this._isLoading = true;
    this._isError = false;
    this._isLoaded = false;
  }

  setLoadedSuccessMeta(): void {
    this._isLoading = false;
    this._isError = false;
    this._isLoaded = true;
  }

  setLoadedErrorMeta(): void {
    this._isError = true;
    this._isLoading = false;
    this._isLoaded = false;
  }
}
