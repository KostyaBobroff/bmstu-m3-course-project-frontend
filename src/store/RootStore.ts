
import { UserStore } from './UserStore';
import { AirportsStore } from './AirportsStore';


export class RootStore {
  readonly userStore = new UserStore();
  readonly airportsStore = new AirportsStore();
}
