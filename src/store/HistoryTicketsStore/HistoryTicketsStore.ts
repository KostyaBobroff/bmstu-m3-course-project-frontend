import { action, computed, makeObservable, observable } from 'mobx';

import { MetaStore } from 'store/common/MetaStore';


import urls from '../../config/urls';
import api from '../../utils/api';
import { ApiResponse } from '../types';

import { HistoryTicketModel, HistoryTicketResponseType } from './models';

type PrivateFields = '_tickets' | '_setTickets';

export class HistoryTicketsStore {
  private _tickets: HistoryTicketModel[] = [];

  readonly meta = new MetaStore();

  constructor() {
    makeObservable<HistoryTicketsStore, PrivateFields>(this, {
      _tickets: observable.ref,

      tickets: computed,

      loadHistory: action.bound,
      _setTickets: action.bound,
    });
  }

  get tickets(): HistoryTicketModel[] {
    return this._tickets;
  }

  private _setTickets(newTickets: HistoryTicketModel[]): void {
    this._tickets = newTickets;
  }

  async loadHistory(): Promise<boolean> {
    this.meta.setLoadedStartMeta();
    try {
      const { response }: ApiResponse<HistoryTicketResponseType[]> = await api(
        urls.history,
        'GET'
      );

      if (response) {
        console.log(response);
        this._setTickets(response.map((el) => new HistoryTicketModel(el)));
      }
    } catch (e) {
      console.log(e);
    }
    this.meta.setLoadedSuccessMeta();

    return true;
  }
}
