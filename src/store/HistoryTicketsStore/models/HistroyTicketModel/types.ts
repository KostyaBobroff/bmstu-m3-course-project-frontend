import { TicketParams } from '../../../models/TicketModel';

export type HistoryTicketResponseType = {
  surname: string;
  name: string;
  patronymic: string;
  is_paid: boolean;
} & TicketParams;
