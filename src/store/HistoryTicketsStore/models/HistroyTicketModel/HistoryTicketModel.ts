import { TicketModel } from '../../../models/TicketModel';

import { HistoryTicketResponseType } from './types';

export class HistoryTicketModel extends TicketModel {
  readonly name: string;
  readonly surname: string;
  readonly patronymic: string;

  readonly isPaid: boolean;

  constructor(ticket: HistoryTicketResponseType) {
    super(ticket);

    this.isPaid = ticket.is_paid;
    this.name = ticket.name;
    this.surname = ticket.surname;
    this.patronymic = ticket.patronymic;
  }

  get fullName(): string {
    return `${this.surname} ${this.name} ${this.patronymic}`;
  }
}
