import { action, computed, makeObservable, observable } from 'mobx';

import urls from '../../config/urls';
import api from '../../utils/api';
import { MetaStore } from '../common/MetaStore';
import { ApiResponse } from '../types';

import {
  normalizeUserProfile,
  normalizeUserProfileRequest,
} from './normalizers';
import { UserProfileType, UserResponseType } from './types';

type PrivateFields = '_profile' | '_isAuthorized' | '_isAuthorizing';

const mock = {
  email: 'test@test.ru',
  username: 'test',
  name: 'Константин',
  phone: '+79097648976',
  surname: 'Бобров',
};
export class UserStore {
  private _profile: UserProfileType | null = null;
  private _isAuthorized = false;

  private readonly meta = new MetaStore();

  private _isAuthorizing = true;

  constructor() {
    makeObservable<UserStore, PrivateFields>(this, {
      _profile: observable.ref,
      _isAuthorized: observable,
      _isAuthorizing: observable,

      profile: computed,
      isAuthorized: computed,
      isLoading: computed,
      isAuthorizing: computed,

      setIsAuthorized: action,
      setIsAuthorizing: action,
      setProfile: action.bound,
      login: action.bound,
      logout: action.bound,
      loadProfile: action.bound,
      registration: action.bound,

      editProfile: action.bound,
    });
  }

  get profile(): UserProfileType | null {
    return this._profile;
  }

  get isAuthorizing(): boolean {
    return this._isAuthorizing;
  }

  get isAuthorized(): boolean {
    return this._isAuthorized;
  }

  get isLoading(): boolean {
    return this.meta.isLoading;
  }

  setIsAuthorized(flag: boolean): void {
    this._isAuthorized = flag;
  }

  setIsAuthorizing(flag: boolean): void {
    this._isAuthorizing = flag;
  }

  setProfile(newProfile: UserProfileType): void {
    this._profile = newProfile;
  }

  async login({
    username,
    password,
  }: {
    username: string;
    password: string;
  }): Promise<boolean> {
    console.log('login', username, password);
    this.meta.setLoadedStartMeta();
    let result = false;
    try {
      this._profile = await new Promise((resolve) =>
        setTimeout(() => resolve(mock), 500)
      );

      result = Boolean(this._profile);
      this.setIsAuthorized(true);
    } catch (e) {
      result = false;
    }

    this.meta.setLoadedSuccessMeta();
    return result;
  }

  async logout(): Promise<boolean> {
    console.log('logout');
    let result = false;
    try {
      result = await new Promise((resolve) =>
        setTimeout(() => resolve(true), 500)
      );
      this._profile = null;
      this.setIsAuthorized(false);
    } catch (e) {
      result = false;
    }

    return result;
  }

  async loadProfile(): Promise<void> {
    this.setIsAuthorizing(true);
    try {
      const { response }: ApiResponse<UserResponseType> = await api(
        urls.profile,
        'GET'
      );

      if (response) {
        this.setProfile(normalizeUserProfile(response));
        console.log(response);
        this.setIsAuthorized(true);
      } else {
        this.setIsAuthorized(false);
      }
    } catch (e) {
      console.log(e);
      this.setIsAuthorized(false);
    }

    this.setIsAuthorizing(false);
  }

  async registration(
    profile: UserProfileType & { password: string }
  ): Promise<boolean> {
    let isAuthorized = false;
    console.log(profile);
    const params = {
      ...normalizeUserProfileRequest(profile),
      password: profile.password,
    };
    this.meta.setLoadedStartMeta();
    let result = false;
    try {
      const { response }: ApiResponse<UserResponseType> = await api(
        urls.profile,
        'POST',
        params
      );

      if (response) {
        this.setProfile(normalizeUserProfile(response));
        isAuthorized = true;
      }

    } catch (e) {
      result = false;
      isAuthorized = false;
    }

    this.setIsAuthorized(isAuthorized);

    this.meta.setLoadedErrorMeta();
    return isAuthorized;
  }

  async editProfile(data: UserProfileType): Promise<void> {
    this.meta.setLoadedStartMeta();
    const params = normalizeUserProfileRequest(data);
    const { response }: ApiResponse<UserResponseType> = await api(
      urls.profile,
      'PUT',
      params
    );

    console.log('put', response);

    this.meta.setLoadedSuccessMeta();

  }
}
