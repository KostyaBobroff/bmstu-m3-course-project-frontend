import { UserProfileType, UserResponseType } from './types';

export const normalizeUserProfile = (
  data: UserResponseType
): UserProfileType => ({
  username: data.login,
  name: data.name,
  phone: data.phone_number,

  email: data.email,
});

export const normalizeUserProfileRequest = (
  data: UserProfileType
): UserResponseType => ({
  login: data.username,
  phone_number: data.phone,
  email: data.email,
  name: data.name,
});
