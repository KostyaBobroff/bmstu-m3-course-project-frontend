export enum UserGenderEnum {
  male = 'male',
  female = 'female',
}

export type UserResponseType = {
  email: string;
  login: string;
  name: string;
  phone_number: string;
};

export type UserProfileType = {
  username: string;
  name: string;

  phone: string;
  email: string;
};
