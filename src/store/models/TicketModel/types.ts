export type TicketInfo = {
  date: Date | null;
  city: string;
  time: Date | null;
};

export type BuyFormParams = {
  passwordNumbers: string;
  passwordSeries: string;
  name: string;
  patronymic: string;
  surname: string;
  birthDate: Date;
};

export type TicketParams = {
  AirlineName: string;
  airline: string;

  departure_at: string | null;
  destination_at: string | null;

  destination: string;
  destination_airport: string;
  flight_number: string;
  link: string;
  origin: string;
  origin_airport: string;
  price: number;
  return_at: string | null;
  return_transfers: string | null;
  transfers: number;

  duration: string | null;
};

// AirlineName: ""
// airline: ""
// currency_code: "RUB"
// dep_date: "2022-01-09T00:00:00Z"
// dep_time: "0000-01-01T09:10:00+03:00"
// departure_at: null
// destination: "SIP"
// destination_airport: ""
// duration: "02:45:00"
// flight_number: "FV5519"
// is_paid: true
// link: ""
// name: "Векшин"
// origin: "SVO"
// origin_airport: ""
// patronymic: "Дмитриевич"
// price: 250000
// surname: "Роман"
// transfers: 0
//
// AirlineName: "S7 Airlines"
// airline: "S7"
// departure_at: "2021-12-30T11:00:00Z"
// destination: "SVO"
// destination_airport: "SVO"
// duration: "00:35:00"
// flight_number: "S723435"
// link: ""
// origin: "DME"
// origin_airport: "DME"
// price: 339800
// transfers: 0
