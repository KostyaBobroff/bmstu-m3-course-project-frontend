import { MetaStore } from 'store/common/MetaStore';

import { BuyFormParams, TicketInfo, TicketParams } from './types';

export class TicketModel {
  readonly id: number;
  readonly flyingCompanyName: string;
  readonly flightNumber: string;

  readonly flyingCompanyCode: string;

  readonly cost: number;

  readonly departureInfo: TicketInfo;
  readonly arrivalInfo: TicketInfo;

  readonly timeInFlying: string | null;

  readonly departureAirportCode: string;
  readonly arrivalAirportCode: string;

  readonly meta = new MetaStore();

  constructor(ticket: TicketParams) {
    this.flyingCompanyCode = ticket.airline;
    this.flyingCompanyName = ticket.AirlineName;
    this.id = Math.random();
    this.flightNumber = ticket.flight_number;

    this.cost = Number(ticket.price) / 100;

    this.departureInfo = {
      time: ticket.departure_at ? new Date(ticket.departure_at) : null,
      date: ticket.departure_at ? new Date(ticket.departure_at) : null,
      city: `${ticket.origin}`,
    };

    this.arrivalInfo = {
      time: ticket.destination_at ? new Date(ticket.destination_at) : null,
      date: ticket.destination_at ? new Date(ticket.destination_at) : null,
      city: `${ticket.destination}`,
    };

    this.timeInFlying = ticket.duration || null;

    this.departureAirportCode = ticket.origin_airport;
    this.arrivalAirportCode = ticket.destination_airport;
  }

  async buyTicket(params: BuyFormParams[]): Promise<boolean> {
    this.meta.setLoadedStartMeta();

    this.meta.setLoadedSuccessMeta();

    return true;
  }
}
